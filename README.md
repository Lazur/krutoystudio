### This is drupal 7 project with docker container. ###
For run this project follow next steps and execute commands:

**$ cd docker4drupal**

**$ docker-compose up -d**

Make sure all containers are running by executing:

**$ docker-compose ps**

For stop containers:

**$ docker-compose stop**

That's it! Drupal website should be up and running at http://localhost:8000.