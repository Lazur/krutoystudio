<?php
/**
 * @file
 * The primary PHP file for this theme.
 */
function krutoystudio_preprocess_html(&$variables) {
  $options = array(
    'group' => JS_THEME,
  );
  drupal_add_js(drupal_get_path('theme', 'krutoystudio'). '/js/agency.js', $options);
  drupal_add_js(drupal_get_path('theme', 'krutoystudio'). '/js/jquery.easing.min.js', $options);
}
