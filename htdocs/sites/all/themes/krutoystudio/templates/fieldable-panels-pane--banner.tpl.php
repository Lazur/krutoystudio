<!-- Header -->
<header
  style="background-image: url(<?php print file_create_url($field_bg_image[0]['uri']); ?>)">
  <div class="container">
    <div class="intro-text">
      <div class="<?php print $classes; ?>">
        <div
          class="intro-lead-in"><?php print $field_intro[0]['safe_value']; ?></div>
        <div
          class="intro-heading"><?php print $field_text[0]['safe_value']; ?></div>
        <a href="<?php print $field_button[0]['url']; ?>"
           class="page-scroll btn btn-xl"><?php print $field_button[0]['title']; ?></a>
      </div>
    </div>
  </div>
</header>
