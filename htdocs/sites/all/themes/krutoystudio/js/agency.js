(function ($) {
  "use strict"; // Start of use strict
  
  Drupal.behaviors.navMenu = {
    attach: function (context) {
      $('a.page-scroll').bind('click', function (event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
          scrollTop: ($($anchor.attr('href')).offset().top - 50)
        }, 1250, 'easeInOutExpo');
        event.preventDefault();
      });
    }
  };
}(jQuery)); // End of use strict
